FROM caddy:2.4.6-builder-alpine as builder

# https://github.com/caddy-dns, route53
RUN xcaddy build v2.4.6 \
      --with github.com/caddy-dns/route53

# install caddy
FROM caddy:2.4.6-alpine

RUN apk add --no-cache \
  tzdata \
  curl

COPY --from=builder /usr/bin/caddy /usr/bin/caddy

# validate install
# RUN caddy list-modules
RUN caddy version
RUN caddy environ

EXPOSE 80 443 2019
WORKDIR /srv

RUN echo "" > /srv/index.html

ENTRYPOINT ["caddy"]
CMD ["run", "--config", "/etc/caddy/Caddyfile", "--adapter", "caddyfile"]

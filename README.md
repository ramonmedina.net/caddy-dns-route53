# caddy-dns-route53

## How to download

### Docker Pull
```
sudo docker pull registry.gitlab.com/ramonmedina.net/caddy-dns-route53:2.4.6
```

### Build Command

```
sudo docker build -t ramonmedina/caddy-dns-route53:2.4.6 https://gitlab.com/ramonmedina.net/caddy-dns-route53.git#2.4.6
```

## Description
Builds a Docker image of Caddy with the AWS Route53 DNS-01 challenge plugin.

## Usage
To Do: Insert example docker-compose.yml and Caddyfile

## License
The contents of this repository are licensed under [GNU General Public License v3.0](https://gitlab.com/ramonmedina.net/caddy-dns-route53/-/blob/main/LICENSE). Software referenced herein may define diferent licenses.

## Referenced Software
[caddy](https://github.com/caddyserver/caddy)
[xcaddy](https://github.com/caddyserver/xcaddy)
[caddy-dns](https://github.com/caddy-dns)
[Alpine Linux](https://github.com/alpinelinux)
